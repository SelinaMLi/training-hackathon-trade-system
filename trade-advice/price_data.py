import datetime
import logging

import pandas_datareader as pdr

LOG = logging.getLogger(__name__)

def get_price(ticker):
    """
    Gets the closing price of the security associated with the given ticker.

    Parameters
    ----------
    ticker : str
        Ticker of security that you want the yesterday's closing price of

    Returns
    -------
    dictionary
        Dictionary with ticker, date, and price
    """
    start_date = datetime.datetime.now() - datetime.timedelta(1)
    df = None
    try:
        df = pdr.get_data_yahoo(ticker, start_date)
        df['Date'] = df.index
    except Exception as ex:
        LOG.error('Error getting data: ' + str(ex))

    LOG.error('Recieved price data:')
    LOG.error(str(df))

    if (df is None):
        return None

    return {'ticker': ticker,
            'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
            'price': df.iloc[-1]['Close'],
            'success': True}
