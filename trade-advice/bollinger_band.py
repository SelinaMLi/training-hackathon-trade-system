import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader as pdr

import io
import base64
import datetime
import price_data

START_DATE = datetime.datetime.today() - datetime.timedelta(365)
END_DATE = datetime.datetime.today() - datetime.timedelta(1)
WINDOW = 30
WINDOW_MAVG = str(WINDOW) + "d mavg"
WINDOW_STD = str(WINDOW) + "d std"
ADJ_CLOSE = 'Adj Close'
UPPER_BAND = 'Upper Band'
LOWER_BAND = 'Lower Band'

def get_price(ticker):
    """
    Calls price_data's get_price method, only returns price.

    Parameters
    ----------
    ticker : str
        Ticker of security that you want the yesterday's closing price of

    Returns
    -------
    double
        Yesterday's closing price of the security
    """
    return price_data.get_price(ticker)['price']


def generate_rolling_avg(ticker):
    """
    Creates a dataframe containing the rolling average of the security

    Parameters
    ----------
    ticker : str
        Ticker of security that you want the rolling avg dataframe of

    Returns
    -------
    dataframe
        Dataframe containing rolling average from start to end date
    """
    df = pdr.get_data_yahoo(ticker, start=START_DATE, end=END_DATE)

    df[WINDOW_MAVG] = df[ADJ_CLOSE].rolling(window=WINDOW).mean()
    df[WINDOW_STD] = df[ADJ_CLOSE].rolling(window=WINDOW).std()

    return df


def calculate_bands(ticker):
    """
    Creates a dataframe containing the upper and lower bands of the Bollinger bands

    Parameters
    ----------
    ticker : str
        Ticker of security that you want the Bollinger bands of

    Returns
    -------
    dataframe
        Dataframe containing Bollinger bands
    """
    df = generate_rolling_avg(ticker)

    df[UPPER_BAND] = df[WINDOW_MAVG] + (df[WINDOW_STD] * 2)
    df[LOWER_BAND] = df[WINDOW_MAVG] - (df[WINDOW_STD] * 2)

    return df

def plotBollBand(df, security_name):

    """
    Calls price_data's get_price method, only returns price.

    Parameters
    ----------
    df : dataframe
        Dataframe containing the Bollinger bands
    security_name : string
        Name of stock you are trying to plot

    Returns
    -------
    String
        UTF-8 representation of the image
    """
    start = START_DATE.strftime("%m-%d-%y")
    end = END_DATE.strftime("%m-%d-%y")

    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(12,6))
    fig.patch.set_facecolor('#505050')
    ax = fig.add_subplot(111)
    x_axis = df[start:end].index.get_level_values(0)

     # Plot shaded 30 Day Bollinger Band 
    ax.fill_between(x_axis, df[start:end][UPPER_BAND], df[start:end][LOWER_BAND], color='black', alpha=0.6)
    ax.plot(x_axis, df[start:end][ADJ_CLOSE], color='#4fb9ff', lw=2)
    ax.plot(x_axis, df[start:end][WINDOW_MAVG], color='white', lw=2)
    
     # Set Title & Show the Image
    title = str(WINDOW) + ' Day Bollinger Band For ' + security_name
    ax.set_title(title, color='white')
    ax.set_xlabel('Date (Year/Month)', color='white')
    ax.set_ylabel('Price', color="white")
    ax.tick_params(colors='white')
    ax.set_facecolor('#505050')
    ax.patch.set_facecolor('#505050')

    plt.tight_layout()

    graph_IObytes = io.BytesIO()
    plt.savefig(graph_IObytes, format="png", facecolor=fig.get_facecolor())
    graph_IObytes.seek(0)
    graph_string = base64.b64encode(graph_IObytes.read()).decode("utf-8")

    return graph_string

def advice(ticker):
    """
    Returns advice on whether to buy, sell, or hold security based on Bollinger bands

    Parameters
    ----------
    ticker : str
        Ticker of security that you want advice for

    Returns
    -------
    String
        Advice based on Bollinger bands
    """
    price = get_price(ticker)
    df = calculate_bands(ticker)

    if price > df.iloc[-1][UPPER_BAND]:
        return 'sell'
    elif price < df.iloc[-1][LOWER_BAND]:
        return 'buy'
    return 'hold'
