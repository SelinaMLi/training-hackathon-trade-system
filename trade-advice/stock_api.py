from datetime import datetime
import logging
import bollinger_band
import price_data
import base64
import json

from flask import Flask, render_template, request
from flask_restplus import Resource, Api
from flask_cors import CORS, cross_origin

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@api.route('/advice/<string:ticker>') 
class Advice(Resource):

    @cross_origin()
    def get(self, ticker):
        """
        API endpoint for getting advice and graph image in UTF-8 representation of b64 encoded image

        Parameters
        ----------
        ticker : str
            Ticker of security that you want the advice/graph of

        Returns
        -------
        dictionary
            Dictionary containing the ticker, advice, and string representation of graph
        """
        print('Requesting advice for: ' + str(ticker))

        if price_data.get_price(ticker) is None:
            return {'ticker': ticker, 'advice': None, 'image': None, 'success': False}

        advice = bollinger_band.advice(ticker)
        df = bollinger_band.calculate_bands(ticker)
        graph_string = bollinger_band.plotBollBand(df, ticker)

        return {'ticker': ticker, 'advice': advice, 'image': graph_string, 'success': True}

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=8084)
