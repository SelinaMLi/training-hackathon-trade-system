import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../services/crud.service'
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms'
import { formatDate, formatNumber } from '@angular/common'
import { LivePriceService } from 'src/app/services/live-price.service';
import { LivePrice } from 'src/app/model/livePrice'
import { UpdateMessageService } from 'src/app/services/update-message.service';
import { Stock } from 'src/app/model/stock'
import { PortfolioService } from 'src/app/services/portfolio.service'

@Component({
  selector: 'app-add-trade',
  templateUrl: './add-trade.component.html',
  styleUrls: ['./add-trade.component.css']
})
export class AddTradeComponent implements OnInit {
  tradeForm: FormGroup;  
  addTradeSuccess = false;
  addTradeFail = false;
  sellTradeFail = false;

  ngOnInit(): void {
    this.tradeForm = this.fb.group({
      stockTicker: ['',Validators.required],
      stockQuantity: ['',Validators.required],
      requestedPrice: ['',Validators.required],
      side: ['',Validators.required],
      orderType:['',Validators.required]
    })
  }

  constructor( 
    public fb: FormBuilder,
    public crudService: CrudService,
    public livePriceService: LivePriceService,
    private updateMessageService: UpdateMessageService,
    private portfolioService: PortfolioService
  ){}

  onOrderTypeSelect(selectedOrderType){
    if(selectedOrderType == "MarketOrder"){
      this.livePriceService.getPrice(this.tradeForm.get('stockTicker').value).subscribe((res : LivePrice) => {
        let formattedPrice = formatNumber(res.price_data[0][1],'en',"1.2-2").toString().replace(/,/g,"");
        this.tradeForm.get('requestedPrice').setValue(formattedPrice);
      })
      this.tradeForm.get('requestedPrice').disable();
    }
    else if(selectedOrderType == "LimitOrder"){
      this.tradeForm.get('requestedPrice').reset();
      this.tradeForm.get('requestedPrice').enable();
    }
  }

  /**
   * Submits add trade form if the form is valid. Sets respective errors if there is any.
   */
  submitForm() {
    if(this.tradeForm.valid){
        this.portfolioService.getStockByTicker(this.tradeForm.get('stockTicker').value).subscribe((data: Stock[]) => {
          if (this.tradeForm.get('side').value  == "SELL") {
            if (data.length == 0) {
              this.sellTradeFail = true;
              this.clearSubmitMessage();
              return;
            } 
            else if (data[0].quantity < this.tradeForm.get('stockQuantity').value) {
              console.log("can't sell");
              this.sellTradeFail = true;
              this.clearSubmitMessage();
              return;
            }
          }
          
          //submit trade and display success message
          const dataForSubmit = this.tradeForm.value;
          dataForSubmit.requestedPrice = this.tradeForm.get('requestedPrice').value;
          dataForSubmit.dateCreated = formatDate(new Date(), 'yyyy-MM-dd', 'en');
          dataForSubmit.status = 'CREATED';
          
          this.crudService.addTrade(dataForSubmit).subscribe(res => {});
          //notifies trade-history component that a new trade has been added.
          this.updateMessageService.notifyTradesUpdate(this.tradeForm.get('stockTicker').value);
    
          this.tradeForm.reset();
          this.addTradeSuccess = true;
          this.clearSubmitMessage();
       });
    } 
    else{
      this.addTradeFail = true;
      this.clearSubmitMessage();
    }

    this.clearSubmitMessage();
  }

  clearSubmitMessage(){
    setTimeout(function(){
      this.addTradeFail = false;
      this.addTradeSuccess = false;
      this.sellTradeFail = false;
    }.bind(this), 3000);
  }
}
