import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription, timer } from 'rxjs';
import { LivePrice } from 'src/app/model/livePrice';
import { Ticker } from 'src/app/model/ticker';
import { LivePriceService } from 'src/app/services/live-price.service';
import { WatchlistService } from 'src/app/services/watchlist.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {

  //table related properties
  subscription: Subscription;
  watchlist: Ticker[] = [];
  watchlistDataSource = new MatTableDataSource<Ticker>();
  priceMap: Map<String, number> = new Map<string, number>();
  displayedColumns: string[] = ['ticker','livePrice','action'];

  //form related properties
  public watchlistAddForm : FormGroup;
  public submitFormFail = false;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public watchlistService: WatchlistService, 
    public livePriceService: LivePriceService,
    public fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.watchlistAddForm = this.fb.group({
      ticker: ['',Validators.required]
    });

    this.refresh();

    this.subscription = timer(0, 3000).subscribe(() =>{
      this.watchlist.forEach(ticker => {
        this.getLivePrice(ticker.ticker);
      });
    });
  }

  ngOnDestroy():void{
    this.subscription.unsubscribe();
  }

  refresh(){
    this.subscription = this.watchlistService.getList().subscribe((data: Ticker[]) => {
      this.watchlist = data;
      this.watchlistDataSource.data = data;
      this.watchlistDataSource.paginator = this.paginator;
      this.watchlistDataSource.sort = this.sort;
    });
  }

  getLivePrice(ticker: String) {
    this.livePriceService.getPrice(ticker).subscribe((res: LivePrice) => {
      this.priceMap.set(ticker, (res.price_data[0][1]));
    })
  };

  submitForm() {
    if(this.watchlistAddForm.valid){
      let ticker = this.watchlistAddForm.get('ticker').value;
      let found = this.watchlist.find((existingTicker) => {return existingTicker.ticker == ticker});
      if(found != null){
        this.submitFormFail = true;
        this.watchlistAddForm.reset();
        this.clearSubmitMessage();
        return;
      }
      else{
        this.watchlistService.addToList(ticker).subscribe(res => {});
        this.refresh();
        this.watchlistAddForm.reset();
      }
    }
    else{
      this.submitFormFail = true;
      this.clearSubmitMessage();
    }
  }

  deleteTicker(ticker) {
    this.watchlistService.deleteFromList(ticker).subscribe(res=>{
      this.refresh();
    });
  }

  clearSubmitMessage(){
    setTimeout(function(){
      this.submitFormFail = false;
    }.bind(this), 3000);
  }
}
