import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChartOptions, ChartType } from 'chart.js';
import { LivePrice } from 'src/app/model/livePrice';
import { LivePriceService } from 'src/app/services/live-price.service';

@Component({
  selector: 'app-stock-data',
  templateUrl: './stock-data.component.html',
  styleUrls: ['./stock-data.component.css']
})
export class StockDataComponent implements OnInit {

  public stockPickerForm : FormGroup;
  public showStockDataGraph = false;

  public priceOverTime : LivePrice;
  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    elements:{
      line:{
        tension: 0
      }
    }
  };
  public lineChartLabels = [];
  public lineChartData = [];
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];
  public lineChartColors: Array < any > = [{
    backgroundColor: 'rgba(84, 209, 77, 0.3)'
  }];

  constructor(public livePriceService: LivePriceService, public fb: FormBuilder) { }

  ngOnInit(): void {
    this.stockPickerForm = this.fb.group({
      stockTicker: ['',Validators.required],
      period: ['',Validators.required]
    })
  }

  submitForm() {
    let ticker = this.stockPickerForm.get('stockTicker').value;
    let period = this.stockPickerForm.get('period').value;
    this.livePriceService.getPricesOverPeriod(ticker,period).subscribe((res => {
      this.priceOverTime = res;
      this.showStockDataGraph = true;

      //populating graph with price data
      this.lineChartLabels = this.priceOverTime.price_data.map(data => data[0]);
      this.lineChartData = this.priceOverTime.price_data.map(data => data[1]);
    }));
  }
}
