import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AdviceService } from 'src/app/services/advice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Advice } from 'src/app/model/advice'

@Component({
  selector: 'app-advice',
  templateUrl: './advice.component.html',
  styleUrls: ['./advice.component.css']
})
export class AdviceComponent implements OnInit {

  adviceForm: FormGroup;
  advice: Advice;
  graph: SafeResourceUrl;
  showAdvice = false;
  showError = false;

  constructor(
    public fb: FormBuilder,
    private router: Router,
    public _DomSanitizationService: DomSanitizer,
    private adviceService: AdviceService) { }

  ngOnInit(): void {
    this.adviceForm = this.fb.group({
      stockTicker: ['',Validators.required]
    })
  }

  submitForm() {
    this.adviceService.advice(this.adviceForm.get('stockTicker').value).subscribe((data: Advice) => {
      this.advice = data
      console.log(this.advice)
      if (this.advice.success == false) {
        this.showError = true;
        this.showAdvice = false;
        return;
      }
      console.log(this.adviceForm.get('stockTicker').value);
      console.log(this.advice)
      this.graph = this._DomSanitizationService.bypassSecurityTrustResourceUrl('data:image/png;base64,' + this.advice.image);
      this.showAdvice = true;
      this.showError = false;
    });
  }

}
