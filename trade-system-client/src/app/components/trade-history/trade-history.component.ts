import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Trade } from 'src/app/model/trade';
import { CrudService } from 'src/app/services/crud.service';
import { LivePriceService } from 'src/app/services/live-price.service'
import { Subscription, timer } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UpdateMessageService } from 'src/app/services/update-message.service';

@Component({
  selector: 'app-trade-history',
  templateUrl: './trade-history.component.html',
  styleUrls: ['./trade-history.component.css']
})
export class TradeHistoryComponent implements OnInit {

  trades: Trade[] = [];
  subscription: Subscription;
  displayedColumns: string[] = ['stockTicker', 'side', 'stockQuantity', 'dateCreated','status','totalInvestment'];
  tradesDataSource = new MatTableDataSource<Trade>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private crudService: CrudService, private livePriceService: LivePriceService, private updateMessageService: UpdateMessageService) {

    this.updateMessageService.tradeSubscriber.subscribe((tradeInfo) => {
      this.refresh();
    });
   }

  ngOnInit(): void {
    this.subscription = timer(0,3000).subscribe(()=>{
      this.refresh();
    })
  }

  ngOnDestroy():void{
    this.subscription.unsubscribe();
    console.log("trade-history unsubscribed to database data");
  }

  //populating trades array with data from api
  refresh(){
    this.crudService.getAll().subscribe((trades: Trade[]) =>{
      this.trades = trades;
      this.tradesDataSource.data = trades;
      this.tradesDataSource.paginator = this.paginator;
      this.tradesDataSource.sort = this.sort;
    });
  }
}
