import { Component, OnInit, ViewChild } from '@angular/core';
import { Stock } from 'src/app/model/stock'
import { PortfolioService } from 'src/app/services/portfolio.service'
import { timer, Observable, Subscription } from 'rxjs';
import { LivePriceService } from 'src/app/services/live-price.service';
import { LivePrice } from 'src/app/model/livePrice';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UpdateMessageService } from 'src/app/services/update-message.service';
import { ChartType, ChartOptions } from 'chart.js';
import { monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { COLORS } from './colors'

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  subscription: Subscription;
  stocks: Stock[] = [];
  stocksDataSource = new MatTableDataSource<Stock>();
  priceMap: Map<String, number> = new Map<string, number>();
  displayedColumns: string[] = ['ticker', 'quantity', 'avgCost', 'valueTotal', 'livePrice', 'profits'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public donutChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'right' }
  };
  public donutChartLabels = [];
  public donutChartData = [];
  public donutChartType: ChartType = 'doughnut';
  public donutChartLegend = true;
  public donutChartPlugins = [];
  public donutChartColors: Array < any > = [{
    backgroundColor: COLORS
  }];

  constructor(private portfolioService: PortfolioService, private livePriceService: LivePriceService, private updateMessageService: UpdateMessageService) {
      this.updateMessageService.tradeSubscriber.subscribe((tradeInfo)=>{
        this.refresh();
      })

      monkeyPatchChartJsTooltip();
      monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.subscription = timer(0,3000).subscribe(()=>{
      this.refresh();
    })

    this.subscription = timer(0, 3000).subscribe(() =>{
      this.stocks.forEach(stock => {
        this.getLivePrice(stock.ticker);
      });
    });
  }

  ngOnDestroy():void{
    this.subscription.unsubscribe();
    console.log("portfolio unsubscribed to database data");
  }

  refresh(){
    this.subscription = this.portfolioService.getStocks().subscribe((data: Stock[]) => {
      this.stocks = data;
      this.stocksDataSource.data = data;
      this.stocksDataSource.paginator = this.paginator;
      this.stocksDataSource.sort = this.sort;

      //populating donut chart value, data consists of percentages for each stock's current value/total value
      let totalValue = this.stocks.map((stock)=> this.priceMap.get(stock.ticker)*stock.quantity).reduce((stockA,stockB)=> stockA+stockB,0);
      this.donutChartLabels = this.stocks.map((stock) => stock.ticker);
      this.donutChartData = this.stocks.map((stock) => (this.priceMap.get(stock.ticker)*stock.quantity)/totalValue);
    });
  }

  //gets live price for a ticker and saves the latest price to priceMap.
  getLivePrice(ticker: String) {
    this.livePriceService.getPrice(ticker).subscribe((res: LivePrice) => {
      //console.log("ticker: "+ticker+" price: "+res.price_data[0][1]);
      this.priceMap.set(ticker, (res.price_data[0][1]));
      //console.log("pricemap val: "+this.priceMap.get(ticker));
    })
  };
}
