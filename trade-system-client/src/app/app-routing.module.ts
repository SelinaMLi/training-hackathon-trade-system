import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/components/home/home.component';
import { AddTradeComponent } from 'src/app/components/add-trade/add-trade.component';
import { TradeHistoryComponent } from 'src/app/components/trade-history/trade-history.component';
import { PortfolioComponent } from 'src/app/components/portfolio/portfolio.component'
import { AdviceComponent } from 'src/app/components/advice/advice.component'
import { StockDataComponent } from './components/stock-data/stock-data.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'addTrade', component: AddTradeComponent }, 
  { path: 'tradeHistory', component: TradeHistoryComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'advice', component: AdviceComponent },
  { path: 'stockData', component: StockDataComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// localhost:4200/home
