export interface Stock {

    id: string;
    ticker: string;
    quantity: number;
    avgCost: number; 
}
