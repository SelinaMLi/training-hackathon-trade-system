export interface Advice {

    ticker: string;
    advice: string;
    image: string;
    success: boolean; 
}
