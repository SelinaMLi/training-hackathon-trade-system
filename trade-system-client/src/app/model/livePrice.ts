export interface LivePrice {

    ticker: String
    price_data: Array<number>

}