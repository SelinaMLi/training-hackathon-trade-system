export interface Trade {

    id: string;
    dateCreated: string;
    stockTicker: string,
    stockQuantity: number;
    requestedPrice: number;
    status: string;
    side: string;
}
