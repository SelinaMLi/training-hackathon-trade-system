import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

/** 
 * Messaging Service class that allows the trade and watchlist components to communicate with eachother. 
 */
export class UpdateMessageService {

  tradeObserver = new Subject();
  public tradeSubscriber = this.tradeObserver.asObservable();

  watchlistObserver = new Subject();
  public watchlistSubscriber = this.watchlistObserver.asObservable();

  constructor() { }

  /**
   * Notifies that a trade has been added by the user.
   * @param trade the trade the user has specified.
   */
  notifyTradesUpdate(trade){
    this.tradeObserver.next(trade);
  }
}
