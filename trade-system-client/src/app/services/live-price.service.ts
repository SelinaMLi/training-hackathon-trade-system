import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import {  throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LivePrice } from '../model/livePrice';

@Injectable({
  providedIn: 'root'
})

/**
 * Service class that communicates with the Live Price Service REST API via REST requests to get the live stock prices.
 */
export class LivePriceService {

  private apiServer = "https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed";

  constructor(private http: HttpClient) { }

  /**
   * Gets the live price of the stock associated with the ticker parameter.
   * @param ticker The ticker of the stock we want live price data for.
   */
  getPrice(ticker) : Observable<LivePrice> {
    return this.http.get<LivePrice>(this.apiServer + "?ticker=" + ticker)
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * 
   * @param ticker The ticker of the stock we want the price data for.
   * @param numDays The number of days we want the closing prices of the stock.
   */
  getPricesOverPeriod(ticker,numDays) : Observable<LivePrice> {
    return this.http.get<LivePrice>(this.apiServer + "?ticker=" + ticker + "&num_days=" + numDays)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * Handles client and server side errors.
   * @param error The error that was caught.
   */
  errorHandler(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      //get client-side error
      errorMessage = error.error.message;
    }
    else{
      //get server-side error
      errorMessage = 'Error Code : ${error.status}\nMessage:${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
