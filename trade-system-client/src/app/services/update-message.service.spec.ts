import { TestBed } from '@angular/core/testing';

import { UpdateMessageService } from './update-message.service';

describe('UpdateMessageService', () => {
  let service: UpdateMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpdateMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
