import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import {  throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Advice } from '../model/advice';

@Injectable({
  providedIn: 'root'
})

/**
 * Service class which makes HTTP REST requests to the Trade Advice API to return information on whether to buy, sell or hold a stock.
 */
export class AdviceService {

  private tradeApiServer = 'http://localhost:8084';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  /**
   * Returns advice on what to do with a stock.
   * @param ticker The ticker of the stock trade advice is being requested on. 
   */
  advice(ticker): Observable<Advice> {
    return this.httpClient.get<Advice>(this.tradeApiServer + '/advice/' + ticker)
    .pipe(
      catchError(this.errorHandler)
    )
  }

   /**
   * Handles client and server side errors.
   * @param error The error that was caught.
   */
  errorHandler(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      //get client-side error
      errorMessage = error.error.message;
    }
    else{
      //get server-side error
      errorMessage = 'Error Code : ${error.status}\nMessage:${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  
}
