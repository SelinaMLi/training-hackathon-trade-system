import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Ticker } from '../model/ticker';

@Injectable({
  providedIn: 'root'
})

/**
 * Service class that communicates with the Watchlist API using http REST requests.
 */
export class WatchlistService {

  private watchlistApiServer = 'http://localhost:8080';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  /**
   * Returns a list fo all stock tickers in the user's watchlist.
   */
  getList() : Observable<Ticker[]> {
    return this.httpClient.get<Ticker[]>(this.watchlistApiServer + '/api/watch')
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * Adds a ticker to the watchlist DB.
   * @param ticker the ticker of the stock to add to the watchlist.
   */
  addToList(ticker): Observable<Ticker> {
    return this.httpClient.post<Ticker>(this.watchlistApiServer + '/api/watch/', JSON.stringify(ticker), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * Deletes a ticker from the watchlist DB.
   * @param ticker the ticker to delete.
   */
  deleteFromList(ticker): Observable<Ticker>{
    return this.httpClient.delete<Ticker>(this.watchlistApiServer + "/api/watch/" + ticker)
    .pipe(
      catchError(this.errorHandler)
    )
  }

   /**
   * Handles client and server side errors.
   * @param error The error that was caught.
   */
  errorHandler(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      //get client-side error
      errorMessage = error.error.message;
    }
    else{
      //get server-side error
      errorMessage = 'Error Code : ${error.status}\nMessage:${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
