import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import {  throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Trade } from '../model/trade';

@Injectable({
  providedIn: 'root'
})

/**
 * Service class for communicating with the Trade System API.
 */
export class CrudService {

  private tradeApiServer = 'http://localhost:8080';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  /**
   * Adds a trade to the TradeDB given user inputs.
   * @param trade The trade to be added to the DB.
   */
  addTrade(trade): Observable<Trade> {
    console.log(JSON.stringify(trade));
    return this.httpClient.post<Trade>(this.tradeApiServer + '/api/trades/', JSON.stringify(trade), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  /**
   * Finds and returns a trade from the Trade DB given it's ID. 
   * @param id The id of the trade
   */
  getTradeById(id) : Observable<Trade> {
    return this.httpClient.get<Trade>(this.tradeApiServer + '/api/trades/' + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  /**
   * Get's a list of all trades from the Trade DB.
   */
  getAll(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>(this.tradeApiServer + '/api/trades/')
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  /**
   * Given an id, deletes the trade with that id from the DB.
   * @param id the id of the trade to be deleted.
   */
  deleteTrade(id){
    return this.httpClient.delete<Trade>(this.tradeApiServer + '/api/trades' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * Updates a trade's fields given the id of the trade to be updated. 
   * @param id the id of the trade to be updated.
   * @param trade The fields of the trade and what value they should be updated to.
   */
  update(id, trade): Observable<Trade> {
    return this.httpClient.put<Trade>(this.tradeApiServer + '/api/trades/' + id, JSON.stringify(trade), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

   /**
   * Handles client and server side errors.
   * @param error The error that was caught.
   */
  errorHandler(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      //get client-side error
      errorMessage = error.error.message;
    }
    else{
      //get server-side error
      errorMessage = 'Error Code : ${error.status}\nMessage:${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
