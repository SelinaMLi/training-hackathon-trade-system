import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import {  throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Stock } from '../model/stock';

@Injectable({
  providedIn: 'root'
})

/**
 * Service class that communicates with the Portfolio REST API via HTTP REST Requests.
 */
export class PortfolioService {

  private apiServer = "http://localhost:8080";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json'
    })
  }
  constructor(private http: HttpClient) { }

  /**
   * Get's all the stocks in the user's portfolio and returns them as an Observable list of stocks.
   */
  getStocks() : Observable<Stock[]> {
    return this.http.get<Stock[]>(this.apiServer + "/api/stocks/")
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * Given a ticker returns the stock within the portfolio DB with that ticker if it exists. 
   * @param ticker the ticker of the stock.
   */
  getStockByTicker(ticker) : Observable<Stock[]> {
    return this.http.get<Stock[]>(this.apiServer + "/api/stocks/ticker/" + ticker)
    .pipe(catchError(this.errorHandler));
  }

   /**
   * Handles client and server side errors.
   * @param error The error that was caught.
   */
  errorHandler(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      //get client-side error
      errorMessage = error.error.message;
    }
    else{
      //get server-side error
      errorMessage = 'Error Code : ${error.status}\nMessage:${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

