PUT http://localhost:8080/api/trades/5f5b87663b23cb3847902448 HTTP/1.1
content-type: application/json

{
    "stockQuantity": 300,
    "status": "FILLED",
    "stockTicker": "TSLA",
    "requestedPrice": 350.67
}