package com.conygre.spring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThrows;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.entities.Ticker;
import com.conygre.spring.service.WatchListService;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

public class TestWatchListController {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";
    public static final String BAD_ID = "5f46a3d545bee629d17fd7b1";

    public static final String TEST_TICKER = "AAPL";
    public static final String BAD_TICKER = "XXXX";

    private WatchListController controller;

    @Before
    public void setUp() {
        ObjectId ID = new ObjectId(TEST_ID);
        Ticker ticker = new Ticker(TEST_TICKER);

        List<Ticker> watchList = new ArrayList<>();
        watchList.add(ticker);

        WatchListService service = mock(WatchListService.class);
        when(service.getWatchList()).thenReturn(watchList);
        when(service.getTicker(TEST_TICKER)).thenReturn(Optional.of(ticker));
        
        controller = new WatchListController(service);

    }

    @Test
    public void testGetWatchList() {
        Iterable<Ticker> tickers = controller.getWatchList();
        Stream<Ticker> stream = StreamSupport.stream(tickers.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testGetById() {
        Optional<Ticker> ticker = controller.getByTicker(TEST_TICKER);
        assertThat(ticker.isPresent(), equalTo(true));
    }

    @Test
    public void testGetByIdFail() {
        assertThrows(ResponseStatusException.class, () -> {
            Optional<Ticker> ticker = controller.getByTicker(BAD_TICKER);
        });

    }    
}