package com.conygre.spring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThrows;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.controller.TradeController;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeSide;
import com.conygre.spring.entities.TradeStatus;
import com.conygre.spring.service.TradeService;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

public class TestTradeController {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";
    public static final String BAD_ID = "5f46a3d545bee629d17fd7b1";
    
    public static final String DATE_STRING = "2020-09-20";
    public static LocalDate DATE;

    public static final String TEST_TICKER = "AAPL";

    public static final TradeStatus CREATED_STATUS = TradeStatus.CREATED;

    public static final TradeSide BUY_SIDE = TradeSide.BUY;
    
    public static final int QUANTITY_1 = 100;
    public static final int QUANTITY_2 = 200;

    public static final double PRICE = 499.23;

    public static final ObjectId TEST_ID_OBJID = new ObjectId(TEST_ID);
    public static final ObjectId BAD_ID_OBJID = new ObjectId(BAD_ID);

    private TradeController controller;

    @BeforeAll
    public void beforeAllSetup() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DATE = LocalDate.parse(DATE_STRING, dtf);
    }

    @Before
    public void setUp() {
        ObjectId ID = new ObjectId(TEST_ID);
        Trade trade1 = new Trade(DATE, TEST_TICKER, QUANTITY_1, PRICE, CREATED_STATUS, BUY_SIDE);

        List<Trade> trades = new ArrayList<>();
        trades.add(trade1);

        TradeService service = mock(TradeService.class);
        when(service.getTrades()).thenReturn(trades);
        when(service.getTradeById(ID)).thenReturn(Optional.of(trade1));
        when(service.getTradeById(BAD_ID_OBJID)).thenReturn(Optional.empty());
        
        controller = new TradeController(service);

    }

    @Test
    public void testGetAllTrades() {
        Iterable<Trade> trades = controller.getAllTrades();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testGetById() {
        Optional<Trade> trade = controller.getById(TEST_ID_OBJID);
        assertThat(trade.isPresent(), equalTo(true));
    }

    @Test
    public void testGetByIdFail() {
        assertThrows(ResponseStatusException.class, () -> {
            Optional<Trade> trade = controller.getById(BAD_ID_OBJID);
        });

    }    
}