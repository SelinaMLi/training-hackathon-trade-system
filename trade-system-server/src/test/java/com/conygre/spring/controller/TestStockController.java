package com.conygre.spring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThrows;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.entities.Stock;
import com.conygre.spring.service.StockService;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

public class TestStockController {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";
    public static final String BAD_ID = "5f46a3d545bee629d17fd7b1";

    public static final String TEST_TICKER = "AAPL";
    
    public static final int QUANTITY_1 = 100;
    public static final int QUANTITY_2 = 200;

    public static final double AVG_COST = 499.23;

    public static final ObjectId TEST_ID_OBJID = new ObjectId(TEST_ID);
    public static final ObjectId BAD_ID_OBJID = new ObjectId(BAD_ID);

    private StockController controller;

    @BeforeAll
    public void beforeAllSetup() {
        
    }

    @Before
    public void setUp() {
        ObjectId ID = new ObjectId(TEST_ID);
        Stock stock1 = new Stock(TEST_TICKER, QUANTITY_1, AVG_COST);

        List<Stock> stocks = new ArrayList<>();
        stocks.add(stock1);

        StockService service = mock(StockService.class);
        when(service.getStocks()).thenReturn(stocks);
        when(service.getStockById(TEST_ID_OBJID)).thenReturn(Optional.of(stock1));
        when(service.getStockById(BAD_ID_OBJID)).thenReturn(Optional.empty());
        
        controller = new StockController(service);

    }

    @Test
    public void testGetAllTrades() {
        Iterable<Stock> stocks = controller.getAllStocks();
        Stream<Stock> stream = StreamSupport.stream(stocks.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testGetById() {
        Optional<Stock> stock = controller.getById(TEST_ID_OBJID);
        assertThat(stock.isPresent(), equalTo(true));
    }

    @Test
    public void testGetByIdFail() {
        assertThrows(ResponseStatusException.class, () -> {
            Optional<Stock> stock = controller.getById(BAD_ID_OBJID);
        });

    }    
}