package com.conygre.spring.data;

import java.util.Optional;
import com.conygre.spring.entities.Ticker;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WatchListRepository extends MongoRepository<Ticker, ObjectId>{
    Optional<Ticker> findByTicker(String ticker);

	void deleteByTicker(String ticker);
}