package com.conygre.spring.data;

import com.conygre.spring.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade,ObjectId>{
    
}