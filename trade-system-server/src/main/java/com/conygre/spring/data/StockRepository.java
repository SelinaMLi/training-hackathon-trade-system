package com.conygre.spring.data;

import java.util.List;

import com.conygre.spring.entities.Stock;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockRepository extends MongoRepository<Stock, ObjectId> {
    List<Stock> findByTicker(String ticker);
}
