package com.conygre.spring.entities;

public enum TradeSide {
    BUY("BUY"), 
    SELL("SELL");

    private String state;

    private TradeSide(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
}
