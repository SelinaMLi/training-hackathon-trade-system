package com.conygre.spring.entities;

public enum TradeStatus{
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED"),
    CANCELLED("CANCELLED");

    private String state;

    private TradeStatus(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
}
