package com.conygre.spring.entities;

import java.time.LocalDate;
import com.conygre.spring.util.IDToString;
import com.conygre.spring.util.StringToID;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Represents a trade object
 * @author Selina Li, Karen Wood, Jason Shin
 * @version 1.0
 */
@Document
public class Trade {
    
    @Id
    @JsonSerialize(converter=IDToString.class)
	@JsonDeserialize(converter=StringToID.class)
    private ObjectId id;
    private LocalDate dateCreated;
    private String stockTicker;
    private int stockQuantity;
    private double requestedPrice;
    private TradeStatus status;
    private TradeSide side;

    public Trade(LocalDate dateCreated, String stockTicker, int stockQuantity, double requestedPrice, TradeStatus status, TradeSide side) {
        this.dateCreated = dateCreated;
        this.stockTicker = stockTicker;
        this.stockQuantity = stockQuantity;
        this.requestedPrice = requestedPrice;
        this.status = status;
        this.side = side;
    }

    public Trade() {}

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public TradeSide getSide() {
        return side;
    }

    public void setSide(TradeSide side) {
        this.side = side;
    }
   

}