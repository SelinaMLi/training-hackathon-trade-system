package com.conygre.spring.entities;

import com.conygre.spring.util.IDToString;
import com.conygre.spring.util.StringToID;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stock {
    @Id
    @JsonSerialize(converter=IDToString.class)
    @JsonDeserialize(converter=StringToID.class)
    private ObjectId id;
    private String ticker;
    private int quantity;
    private double avgCost;

    public Stock() {}

    public Stock(String ticker, int quantity, double avgCost) {
        this.ticker = ticker;
        this.quantity = quantity;
        this.avgCost = avgCost;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

}