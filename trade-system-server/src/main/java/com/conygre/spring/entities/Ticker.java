package com.conygre.spring.entities;

import com.conygre.spring.util.IDToString;
import com.conygre.spring.util.StringToID;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Ticker {
    @Id
    @JsonSerialize(converter=IDToString.class)
    @JsonDeserialize(converter=StringToID.class)
    private ObjectId id;

    String ticker;

    public Ticker(String ticker) {
        this.ticker = ticker;
    }
    public Ticker() {  }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

}
