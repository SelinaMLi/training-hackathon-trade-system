package com.conygre.spring.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import com.conygre.spring.entities.Stock;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeStatus;
import com.conygre.spring.entities.TradeSide;

public class JSONParserUtil {
    
    public static Trade JSONToTrade(Map<String, String> tradeMap) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateCreated = tradeMap.get("dateCreated") == null ? null : LocalDate.parse(tradeMap.get("dateCreated"), dtf);
        String stockTicker = tradeMap.get("stockTicker"); 
        int stockQuantity = tradeMap.get("stockQuantity") == null ? 0 : Integer.parseInt(tradeMap.get("stockQuantity"));
        double requestedPrice = tradeMap.get("requestedPrice") == null ? 0.0 : Double.parseDouble(tradeMap.get("requestedPrice"));
        TradeStatus status = tradeMap.get("status") == null ? null : TradeStatus.valueOf(tradeMap.get("status"));
        TradeSide side = tradeMap.get("side") == null ? null : TradeSide.valueOf(tradeMap.get("side"));
        return new Trade(dateCreated, stockTicker, stockQuantity, requestedPrice, status, side);
    }

    public static boolean missingFields(Map<String, String> tradeMap) {
        String[] requiredKeys = new String[]{"dateCreated", "stockTicker", "stockQuantity", "requestedPrice", "status", "side"};
        for (String key : requiredKeys) {
            if (!tradeMap.containsKey(key)) {
                return true;
            }
        } 
        return false;
    }


    public static Stock JSONToStock(Map<String, String> stockMap) {
        String ticker = stockMap.get("ticker");
        int quantity = stockMap.get("quantity") == null ? null : Integer.parseInt(stockMap.get("quantity"));
        double avgCost = stockMap.get("avgCost") == null ? null : Double.parseDouble(stockMap.get("avgCost"));
        return new Stock(ticker, quantity, avgCost);
    }
   
}
