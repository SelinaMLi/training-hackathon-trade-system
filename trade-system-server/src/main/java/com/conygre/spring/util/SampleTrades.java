package com.conygre.spring.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeSide;
import com.conygre.spring.entities.TradeStatus;

public class SampleTrades {

    private static final int SIZE_OF_DATA = 25;

    private static final String[] DATES_STRING = new String[]{"2019-12-03", "2019-11-08", "2019-10-22", "2019-09-22", "2019-08-21", "2018-05-23", "2017-09-08",
                                                 "2020-03-13", "2020-07-15", "2014-04-03", "2016-06-28", "2011-04-30", "2012-09-06", "2014-02-02", 
                                                 "2018-11-22", "2009-05-05", "2014-04-13", "2013-11-22", "2020-01-09", "2016-02-22", "2011-11-11", 
                                                 "2019-12-03", "2020-09-17", "2020-09-17", "2020-09-17"};
    private static List<LocalDate> DATES = new ArrayList<>();
    private static final String[] TICKERS = new String[]{"C", "TSLA", "AAPL", "MDB", "FB", "BABA", "MSFT", "SQ", "V", "JNJ", "KO", "C", 
                                                        "TSLA", "AAPL", "MDB", "FB", "BABA", "MSFT", "SQ", "V", "JNJ", "KO", "C", "AAPL", "TSLA"};
    private static final int[] QUANTITIES = new int[] {24, 21, 13, 3, 6, 3, 7, 9, 4, 3, 5, 4, 3, 2, 1, 3, 20, 13, 14, 20, 22, 21, 2, 3, 4};
    private static final double[] PRICES = new double[] {40.20, 399.33, 201.20, 201.65, 265.82, 265.72, 192.91, 135.33, 245.27, 132.17, 59.55,
                                                         39.20, 301.33, 241.11, 207.22, 274.28, 235.79, 188.17, 119.11, 223.26, 133.12, 53.57,
                                                         42.20, 319.22, 207.20};                                                  
    public static Trade[] TRADES = new Trade[SIZE_OF_DATA];
    static {
        for (String date : DATES_STRING) {
            DATES.add(LocalDate.parse(date));
        }
        for (int i = 0; i < SIZE_OF_DATA; i++) {
            TRADES[i] = new Trade(DATES.get(i), TICKERS[i], QUANTITIES[i], PRICES[i], TradeStatus.CREATED, TradeSide.BUY);
        }
    }

    
    
    
}
