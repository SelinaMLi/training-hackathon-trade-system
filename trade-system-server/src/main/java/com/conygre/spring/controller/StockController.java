package com.conygre.spring.controller;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import com.conygre.spring.entities.Stock;
import com.conygre.spring.entities.TradeSide;
import com.conygre.spring.service.StockService;
import com.conygre.spring.util.JSONParserUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller layer for Stock API.
 * 
 * @author Selina Li, Karen Wood, Jason Shin
 * @version 1.0
 */
@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/stocks")
public class StockController {
    
    @Autowired
    private StockService service;

    public StockController(StockService service) {
        this.service = service;
    }

     /**Calls service layer to get all trades.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.GET)
    public Collection<Stock> getAllStocks() {
        return service.getStocks();
    }

    @RequestMapping(method=RequestMethod.GET, value="/ticker/{ticker}")
    public Collection<Stock> getStockByTicker(@PathVariable("ticker") String ticker) {
        return service.getStockByTicker(ticker);
    }

    /**Calls service layer to get stock by ID.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.GET, value="/{id}")
    public Optional<Stock> getById(@PathVariable("id") ObjectId id) {
        Optional<Stock> stock = service.getStockById(id);
        if (!stock.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return service.getStockById(id);
    }

    //TODO: Add RequestMappings for Stock info.

    /** Calls the service layer to add  a stock*/
    @RequestMapping(method=RequestMethod.POST)
    public void addStock(@RequestBody Stock stock) {
        //TODO: add checks for stock validity
        try {
            service.addToStocks(stock);
        } catch(Exception e) {
            System.out.println(e);
        }
    }

    /**Calls service layer to get update stock by Id.
     * @see com.conygre.spring.service.StockService.
    */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public void updateStockById(@RequestBody Stock stock, @PathVariable("id") ObjectId id) {
        service.updateStockById(id, stock);
    }

    @RequestMapping(method=RequestMethod.PUT)
    public void updateStockPortfolio(@RequestBody Map<String, String> stockMap) {
        Stock stock = JSONParserUtil.JSONToStock(stockMap);
        TradeSide side = TradeSide.valueOf(stockMap.get("side"));

        try {
        service.updateStockPortfolio(stock, side);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    /**Calls service layer to delete a stock by ID.
     * @see com.conygre.spring.service.StockService.
     */
    @RequestMapping(method=RequestMethod.DELETE, value="/{id}")
    public void deleteStockById(@PathVariable("id") ObjectId id) {
        service.deleteStockById(id);
    }

}
