package com.conygre.spring.controller;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.entities.Ticker;
import com.conygre.spring.service.WatchListService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/watch")
public class WatchListController {
    
    @Autowired
    private WatchListService service;

    public WatchListController() {
    }

    public WatchListController (WatchListService service) {
        this.service = service;
    }

    /**Calls service layer to get watchlist.
     * @see com.conygre.spring.service.WatchListService.
     */
    @RequestMapping(method=RequestMethod.GET)
    public Collection<Ticker> getWatchList(){
        return service.getWatchList();
    }

    /**Calls service layer to get ticker.
     * @see com.conygre.spring.service.WatchListService.
     */
    @RequestMapping(method=RequestMethod.GET, value="/{ticker}")
    public Optional<Ticker> getByTicker(@PathVariable("ticker") String name) {
        Optional<Ticker> ticker = service.getTicker(name);
        if (!ticker.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return service.getTicker(name);
    }

    @RequestMapping(method=RequestMethod.POST) 
    public void addToWatchList(@RequestBody Ticker ticker) {
        service.addToWatchList(ticker);
    }

     /**Calls service layer to delete a ticker.
     * @see com.conygre.spring.service.WatchListService.
     */
    @RequestMapping(method=RequestMethod.DELETE, value="/{ticker}")
    public void deleteTicker(@PathVariable("ticker") String ticker) {
        service.deleteTicker(ticker);
    }

}
