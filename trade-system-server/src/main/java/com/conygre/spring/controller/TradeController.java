package com.conygre.spring.controller;

import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.service.TradeService;
import com.conygre.spring.util.JSONParserUtil;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

 
/**Controller layer for trade API.
 * @author Selina Li, Karen Wood, Jason Shin
 * @version 1.0
 */
@CrossOrigin(origins = "*" )
@RestController
@RequestMapping("/api/trades")
public class TradeController {

    @Autowired
    private TradeService service;

    public TradeController() {
    }

    public TradeController (TradeService service) {
        this.service = service;
    }

    /**Calls service layer to get all trades.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.GET)
    public Collection<Trade> getAllTrades(){
        return service.getTrades();
    }

    @RequestMapping(value="/populate", method=RequestMethod.GET)
    public String populateSampleData() {
        service.addSampleData();
        return "Success";
    }


    /**Calls service layer to get trade by ID.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.GET, value="/{id}")
    public Optional<Trade> getById(@PathVariable("id") ObjectId id) {
        Optional<Trade> trade = service.getTradeById(id);
        if (!trade.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return service.getTradeById(id);
    }

    /**Calls service layer to add trade.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.POST)
    public void addTrade(@RequestBody Map<String, String> tradeMap) {
        
        // Date date = Date.parse(tradeMap.get("dateCreated"));
        if (JSONParserUtil.missingFields(tradeMap)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing required fields for trade");
        }
        try {
           service.addToTrades(JSONParserUtil.JSONToTrade(tradeMap));
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid date format");
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid price or quantity");
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid trade status");
        }
    }

    /**Calls service layer to get update trade by ID.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.PUT, value="/{id}")
    public void updateTradeById(@RequestBody Map<String, String> tradeMap, @PathVariable("id") ObjectId id) {
        service.updateTradeById(id, JSONParserUtil.JSONToTrade(tradeMap));
    }

    /**Calls service layer to delete a trade by ID.
     * @see com.conygre.spring.service.TradeService.
     */
    @RequestMapping(method=RequestMethod.DELETE, value="/{id}")
    public void deleteTradeById(@PathVariable("id") ObjectId id) {
        service.deleteTradeById(id);
    }
}