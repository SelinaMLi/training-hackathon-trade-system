package com.conygre.spring.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.Stock;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeStatus;
import com.conygre.spring.util.SampleTrades;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Service layer for adding, retrieving, updating, and deleting trade information.
 * @author Selina Li, Karen Wood, Jason Shin
 * @version 1.0
 */
@Service
public class TradeService {
    
    @Autowired
    private TradeRepository dao;

    /**Adds a trade to database.
     * @param trade A trade object to insert.
     */
    public void addToTrades(Trade trade) {

        dao.insert(trade);
    }

    public void addSampleData() {
        for (Trade trade : SampleTrades.TRADES) {
            addToTrades(trade);
        }
    }

    /**Returns all trades in database.
     * @return A collection of all trades in database.
     */
    public Collection<Trade> getTrades() {
        return dao.findAll();
    }

    /**Retrieve a trade from database by its ID.
     * @param id ID of trade.
     * @return Trade of type optional from querying the database.
     */
    public Optional<Trade> getTradeById(ObjectId id) {
        return dao.findById(id);
    }

    /**Updates a trade by its ID given a new trade object. 
     * The method checks to see which properties of the new trade object has been 
     * filled out, and then updates the trade in databse accordingly.
     * @param id ID of trade to update.
     * @param trade Trade entity with new fields intended to replace old information.
     */
    public void updateTradeById(ObjectId id, Trade newTrade) {
        Optional<Trade> oldTradeOptional = getTradeById(id);
        if (oldTradeOptional.isPresent()){
            Trade oldTrade = oldTradeOptional.get();
            if (oldTrade.getStatus().equals(TradeStatus.CREATED)) {
                oldTradeOptional.map(t -> {
                    if(newTrade.getRequestedPrice() != 0.0)  t.setRequestedPrice(newTrade.getRequestedPrice());
                    if(newTrade.getStatus() != null) t.setStatus(newTrade.getStatus());
                    if(newTrade.getStockQuantity() != 0) t.setStockQuantity(newTrade.getStockQuantity());
                    if(newTrade.getStockTicker() != null)  t.setStockTicker(newTrade.getStockTicker());
                    if(newTrade.getSide() != null) t.setSide(newTrade.getSide());
                    return dao.save(t);
                });
            }
        }
    }

    /**
     * Gets the stocks associated with a portfolio.
     * @param id
     */
    public List<Stock> getPortfolioById(ObjectId id) {
        return null;
    }

    /**Deletes a trade by ID.
     * @param id ID of trade to delete.
     */
    public void deleteTradeById(ObjectId id) {
        dao.deleteById(id);
    }

}