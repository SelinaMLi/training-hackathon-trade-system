package com.conygre.spring.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.conygre.spring.data.StockRepository;
import com.conygre.spring.entities.Stock;
import com.conygre.spring.entities.TradeSide;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class StockService {
    @Autowired
    private StockRepository dao;

    /**
     * 
     * @return A collection of all Stocks in the database.
     */
    public Collection<Stock> getStocks() {
        return dao.findAll();
    }

        /**Retrieve a stock from database by its ID.
     * @param id ID of stock.
     * @return Stock of type optional from querying the database.
     */
    public Optional<Stock> getStockById(ObjectId id) {
        return dao.findById(id);
    }

    public Collection<Stock> getStockByTicker(String ticker) {
        return dao.findByTicker(ticker);
    }

    /**Adds a stock to database.
     * @param stock A stock object to insert.
     */
    public void addToStocks(Stock stock) {
        dao.insert(stock);
    }

    /** Updates a stock in the database. 
     * @param stock A stock to edit.
     */
    public void updateStockById(ObjectId id, Stock newStock) {
        Optional<Stock> oldStockOptional = getStockById(id);
        if (oldStockOptional.isPresent()) {
            oldStockOptional.map(s -> {
                if (newStock.getTicker() != null) s.setTicker(newStock.getTicker());
                if (newStock.getQuantity() != 0) s.setQuantity(newStock.getQuantity());
                if (newStock.getAvgCost() != 0.0) s.setAvgCost(newStock.getAvgCost());
                return dao.save(s);
            });
        }
    }

     /** Updates a stock in the database. 
     * @param stock A stock to edit.
     */
    public void updateStockPortfolio(Stock newStock, TradeSide side) {
        // check if the DB already has a stock of this kind:
        System.out.println("Inside Stock Service");
        List<Stock> sameTicker = dao.findByTicker(newStock.getTicker());

        if (sameTicker.size() >= 1) {
            
            // Stock oldStock = sameTicker.get(0);
       

            Stock oldStocks = new Stock(newStock.getTicker(), 0, 0);
            for (int i = 0; i < sameTicker.size(); i++) {
                Stock oldStock = sameTicker.get(i);
                oldStocks.setAvgCost(oldStock.getAvgCost() + oldStocks.getAvgCost());
                oldStocks.setQuantity(oldStock.getQuantity() + oldStocks.getQuantity());  
                dao.delete(oldStock);      
            }
            int newQuantity = side == TradeSide.BUY ? newStock.getQuantity() : -newStock.getQuantity();
   
            // Update new Stock quantity
            oldStocks.setQuantity(oldStocks.getQuantity() + newQuantity);

            //Update avgPrice of the stock.
            oldStocks.setAvgCost(getAvgCost(oldStocks, newStock));
            addToStocks(oldStocks);
            // updateStockById(oldStocks.getId(), oldStocks);

        } else {
            System.out.println("Adding new trade");
            
            // The stock didn't exist in the portfolio yet, so add it.
            if (side == TradeSide.BUY) {
                addToStocks(newStock);
                System.out.println("Success");

            } else  {
                //This is an error.
                System.out.println("can't sell stock you dont own");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot sell a stock you do not own");            
            }
        }
    }
    
    /**Deletes a stock by ID.
     * @param id ID of stock to delete.
     */
    public void deleteStockById(ObjectId id) {
        dao.deleteById(id);
    }

    double getAvgCost(Stock oldStock, Stock newStock)  {
       return ((oldStock.getAvgCost() * oldStock.getQuantity()) + (newStock.getAvgCost() * newStock.getQuantity()))/ (newStock.getQuantity() + oldStock.getQuantity());
    }
}