package com.conygre.spring.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.data.WatchListRepository;
import com.conygre.spring.entities.Ticker;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WatchListService {
    @Autowired
    private WatchListRepository dao;

	public Optional<Ticker> getTicker(String ticker) {
		return dao.findByTicker(ticker);
	}

	public Collection<Ticker> getWatchList() {
		return dao.findAll();
    }
    
     /**Adds a ticker to database.
     * @param stock A ticker object to insert.
     */
    public void addToWatchList(Ticker ticker) {
        dao.insert(ticker);
    }

     /**Deletes a ticker.
     * @param ticker ticker to delete.
     */
    
    public void deleteTicker(String ticker) {
        dao.deleteByTicker(ticker);
    }

}
