package com.conygre.training.tradesimulator.sim;

import java.util.List;
import java.util.Map;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    @Transactional
    public List<Trade> findTradesForProcessing() {
        List<Trade> foundTrades = tradeDao.findByStatus(TradeStatus.CREATED);
        //System.out.println(foundTrades);
        for (Trade thisTrade : foundTrades) {
            thisTrade.setStatus(TradeStatus.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling() {
        List<Trade> foundTrades = tradeDao.findByStatus(TradeStatus.PROCESSING);
        //System.out.println(foundTrades);

        for (Trade thisTrade : foundTrades) {
            if ((int) (Math.random() * 10) > 8) {
                thisTrade.setStatus(TradeStatus.REJECTED);
            } else {
                thisTrade.setStatus(TradeStatus.FILLED);

                Map<Object, Object> payload = Map.of(
                    "ticker", thisTrade.getStockTicker(),   
                    "quantity", thisTrade.getStockQuantity(), 
                    "avgCost", thisTrade.getRequestedPrice(), 
                    "side", thisTrade.getSide());

                RestTemplate template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
                template.put("http://localhost:8080/api/stocks", payload, payload.getClass());
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        LOG.debug("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.debug("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.debug("Found " + tradesForProcessing + " trades to be processed");

    }
}
